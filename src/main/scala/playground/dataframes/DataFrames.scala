package playground.dataframes

import org.apache.spark.sql.{Row, SparkSession}
import org.apache.spark.sql.types._

object DataFrames extends App {

  // creating a SparkSession
  val spark = SparkSession.builder()
    .appName("DataFrames")
    .config("spark.master", "local")
    .getOrCreate()

  // reading a DF
  val carsDF = spark.read
    .format("json")
    .option("inferSchema", "true") // figure out columns structure from the json
    .load("src/main/resources/data/cars.json")

  // showing a DF
  carsDF.show()
  carsDF.printSchema()

  // obtain a schema
  val carsDFSchema = carsDF.schema

  // get rows
  carsDF.take(10).foreach(println)

  // spark types
  val longType = LongType

  // schema
  val carsSchema = StructType(Array(
    StructField("Name", StringType),
    StructField("Miles_per_Gallon", DoubleType),
    StructField("Cylinders", LongType),
    StructField("Displacement", DoubleType),
    StructField("Horsepower", LongType),
    StructField("Weight_in_lbs", LongType),
    StructField("Acceleration", DoubleType),
    StructField("Year", StringType),
    StructField("Origin", StringType)
  ))

  // read a DF with your schema
  val carsDFWithSchema = spark.read
    .format("json")
    .schema(carsSchema)
    .load("src/main/resources/data/cars.json")

  // showing a DF
  carsDFWithSchema.show()
  carsDFWithSchema.printSchema()

  // get rows
  carsDFWithSchema.take(10).foreach(println)

  import spark.implicits._

  val smartphones = Seq(
    ("Samsung", "Galaxy S10", "Android", 12),
    ("Apple", "iPhone X", "iOS", 13),
    ("Nokia", "3310", "THE BEST", 0)
  )

  val smartphonesDF = smartphones.toDF("Make", "Model", "Platform", "CameraMegapixels")
  smartphonesDF.show()

}
